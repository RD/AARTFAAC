#include "Correlator/Filter.h"
#include "Common/BandPass.h"


Filter::Filter(const cu::Device &device, const CorrelatorParset &ps)
:
  ps(ps),
  filter(device, tcc::FilterArgs {
    .nrReceivers         = ps.nrStations(),
    .nrChannels          = ps.nrChannelsPerSubband(),
    .nrSamplesPerChannel = ps.nrSamplesPerChannel(),
    .nrPolarizations     = ps.nrPolarizations(),
    .input               = tcc::FilterArgs::Input {
      .sampleFormat      = ps.nrBitsPerSample() == 16 ? tcc::FilterArgs::Format::i16 :
			   ps.nrBitsPerSample() ==  8 ? tcc::FilterArgs::Format::i8 :
							tcc::FilterArgs::Format::i4,
    },
    .firFilter           = tcc::FilterArgs::FIR_Filter {
      .nrTaps            = NR_TAPS,
    },
    .applyDelays         = ps.delayCompensation(),
    .bandPassCorrection  = ps.correctBandPass() ?
      std::optional<tcc::FilterArgs::BandPassCorrection>(tcc::FilterArgs::BandPassCorrection {
	.weights         = [&] {
	   std::vector<float> bandPassWeights(ps.nrChannelsPerSubband());

	   BandPass::computeCorrectionFactors(bandPassWeights.data(), ps.nrChannelsPerSubband());
	   return bandPassWeights;
	 } (),
      }) : std::optional<tcc::FilterArgs::BandPassCorrection>(std::nullopt),
    .output              = tcc::FilterArgs::Output {
      .sampleFormat      = ps.nrBitsPerSample() == 16 ? tcc::FilterArgs::Format::fp16 :
							tcc::FilterArgs::Format::i8,
    },
  })
#if 0
	 ps.nrStations(),
	 NR_TAPS,
	 ps.nrChannelsPerSubband(),
	 ps.nrSamplesPerChannel(),
	 ps.nrPolarizations(),
	 ps.delayCompensation(),
	 (ps.correctBandPass() ? [&] {
	   std::vector<float> bandPassWeights(ps.nrChannelsPerSubband());

	   BandPass::computeCorrectionFactors(bandPassWeights.data(), ps.nrChannelsPerSubband());
	   return bandPassWeights;
	 } () : static_cast<std::optional<std::vector<float>>>(std::nullopt))
	)
#endif
{
}


void Filter::launchAsync(cu::Stream &stream, cu::DeviceMemory &devOutSamples, const cu::DeviceMemory &devInSamples, const cu::DeviceMemory &devDelaysAtBegin, PerformanceCounter &counter)
{
  PerformanceCounter::Measurement measurement(counter, stream, filter.nrOperations(), 0, 0);
  filter.launchAsync(stream, devOutSamples, devInSamples, ps.delayCompensation() ? devDelaysAtBegin : static_cast<std::optional<cu::DeviceMemory>>(std::nullopt));
}
