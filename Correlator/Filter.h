#if !defined CORRELATOR_FILTER_H
#define CORRELATOR_FILTER_H

#include "Common/PerformanceCounter.h"
#include "Correlator/Parset.h"

#include <cudawrappers/cu.hpp>

#include <libfilter/Filter.h>


class Filter
{
  public:
    Filter(const cu::Device &, const CorrelatorParset &);

    void launchAsync(cu::Stream &stream,
		     cu::DeviceMemory &devCorrectedData,
		     const cu::DeviceMemory &devInSamples,
		     const cu::DeviceMemory &devDelaysAtBegin,
		     PerformanceCounter &counter);

  private:
    const CorrelatorParset &ps;
    tcc::Filter		   filter;
};


#endif
